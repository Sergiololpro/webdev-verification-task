const gulp = require('gulp');
const gutil = require('gulp-util');
const debug = require('gulp-debug');
const del = require('del');
const concat = require('gulp-concat');
const newer = require('gulp-newer');
const sass = require('gulp-sass');
const browserSync = require('browser-sync').create();

const cfg = {
    wwwroot: 'wwwroot/',
    css: {
        src: 'front/css/*.scss',
        dest: 'wwwroot/css'
    },
    js: {
        src: 'front/js/*.js',
        dest: 'wwwroot/js'
    },
    assets: {
        src: ['front/assets/**/*', 'front/*.html'],
        dest: 'wwwroot/assets'
    }
};

// Clean build directory
gulp.task('clean', cb => {
    gutil.log('Cleaning ' + cfg.wwwroot);
    return del([cfg.wwwroot], cb);
});

// Compile SCSS files
gulp.task('scss:compile', () => {
        gutil.log('Compile SASS files to ' + cfg.css.src);
        return gulp.src(cfg.css.src)
            .pipe(debug({title: 'scss:compile'}))
            .pipe(sass().on('error', sass.logError))
            .pipe(concat('app.css'))
            .pipe(gulp.dest(cfg.css.dest));
    }
);

// Copy JS files to wwwRoot
gulp.task('js:copy', () => {
    gutil.log('Copy app JS to ' + cfg.js.dest);
    return gulp.src(cfg.js.src)
        .pipe(newer(cfg.js.dest))     // Copy only changed files to speed up DEV build
        .pipe(debug({title: 'js:copy'}))
        .pipe(gulp.dest(cfg.js.dest));
});

// Copy assets (fonts,img, etc) to wwwRoot
gulp.task('assets:copy', () => {
        gutil.log('Copy app assets to ' + cfg.assets.dest);
        return gulp.src(cfg.assets.src)
            .pipe(newer(cfg.assets.dest))     // Copy only changed files to speed up DEV build
            .pipe(debug({title: 'assets:copy'}))
            .pipe(gulp.dest(cfg.assets.dest));
    }
);

// Watch for changes in source files
gulp.task('watch', () => {

    function fileName(e) {
        return e ? e.path || e : 'unknown';
    }

    gulp.watch(['front/css/*.scss'], gulp.series('scss:compile'))
        .on('change', e => console.log('SCSS file ' + fileName(e) + ' changed. Recompile.'));

    gulp.watch(['front/js/*.js'], gulp.series('js:copy'))
        .on('change', e => console.log('JS file ' + fileName(e) + ' changed. Recompile.'));

    gulp.watch(['front/assets/**/*.*', 'front/*.html'], gulp.series('assets:copy'))
        .on('change', e => console.log('Asset file ' + fileName(e) + ' changed. Updating.'));
});

// Run http server with browser sync
gulp.task('serve', () => {
    browserSync.init({
        //port: 1003,
        server: cfg.wwwroot,
        startPath: 'index.html',
        files: ['assets/**/*', 'js/**/*', '*.ico', '*.html']
    });
    browserSync.watch(cfg.wwwroot + '**/*.*').on('change', browserSync.reload);
});

// Build DEV version of the app
gulp.task('dev', gulp.series('clean', gulp.parallel('scss:compile', 'js:copy', 'assets:copy')));

// Serve DEV version of the app with life reload and watch
gulp.task('serve', gulp.series('dev', gulp.parallel('serve', 'watch')));