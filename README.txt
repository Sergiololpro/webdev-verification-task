PREREQUIREMENTS

    -An account at https://github.com (you need it to fork our project)
    -NodeJS v6.10.x or above (https://nodejs.org) must be installed and 'npm' command must be available in the system

SETUP

    -Fork our project https://gitlab.com/ayrlabsrus-public/webdev-verification-task to your gitlab account (use Fork button)
    -Clone forked repository to your machine
    -Navigate to the folder you cloned to and run 'npm i' (without quotes) to install required node modules
    -Run 'npm run serve'
    -This will build the project, run development server and open index.html in your default browser
    -Once the server is up and running you can start changing project files - browser gets refreshed automatically

REQUIREMENTS

    You're given a SKETCH file with design (https://www.dropbox.com/s/86n6kxqtz5w9uiv/MS_03_MyAyr_Interview.sketch?dl=0).
    If you unable to open SKETCH on your local machine let us know and we'll provide access to AVACODE account with already uploaded SKETCH.
	The sketch file contains 2 views:
	    (1) MS_03_MyAyr_M_2.1_MyDetails_NavigationPanel Copy - profile details form with User's avatar, First/Last Name fields and button.
	    (2) MS_03_MyAyr_M_2.1_MyDetails_NavigationPanel Copy 2 - navigation menu with User's avatar and navigation links

	The GOAL is to create 1 page of HTML+CSS markup
	    -The page must be ADOPTIVE and RESPONSIVE (use 12 cols grid, e.g. from twitter bootstrap)
		-On desktop/tablet the menu (2) must be displayed on the left (3 cols of 12 col grid) and profile details (1) - on the right. Burger icon must be hidden.
		-On mobile profile details (1) must take full width of the page (12 cols). Navigation menu (2) must be initially hidden and appear by clicking 
		 on burger icon in site's header. Second click must hide the menu (2) and show profile details (1) again.
		-Note, than when text input is focused it's border and label color must change to #A18E76 (focused state)
    
    You can change both project structure and build scripts in a way you consider appropriate.
    You're also allowed to use any 3rd party libraries (e.g. jquery, twitter bootstrap, etc).
    
    BONUS GOAL: Create this as React or Angular (v2 or above) application which will handle clicks on navigation menu links 
                and show different views (profile & settings, the last one may be an empty component).

    Once the task is completed create MERGE REQUEST from your project to master branch of ayrlabsrus-public/webdev-verification-task.
    Merge request description format:
        AYR Verification task for web developer.
        Applicant: your name, your email.

ASSESSMENT CRITERIA (WHAT WE EXPECT TO SEE)

    1. Markup should perfectly match design on IE 10 (and higher), FF 50 (and higher) + mobile , Chrome 55 (and higher) + mobile, IOS Safary 9 (and higher) + mobile.
    2. Clear CSS code - a person with minimal knowledge of CSS/HTML must be able to understand and extend it

Regards, AYR Labs Rus Team.

